import express, { Router } from "express";
import { createServer } from "http";
import { Server } from "socket.io";
import mongoose from "mongoose";
import * as boardsController from "./controllers/analytics";
import bodyParser from "body-parser";
import authMiddleware from "./middlewares/auth";
import cors from "cors";
import { collectDefaultMetrics, Registry, Histogram } from 'prom-client';
import { logger } from './utilities/logger';
import opentelemetry from '@opentelemetry/api';
import { tracer, addTraceId } from './utilities/tracing-tempo';
import './utilities/instrumentation';
// import { context, setSpan, getSpan } from '@opentelemetry/api';
import { httpRequestTimer, totalHttpRequestDuration, totalHttpRequestCount } from "./utilities/prometheus";

const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer, {
  cors: {
    origin: "*",
  },
});

// Create a Registry to register the metrics
const register = new Registry();
register.setDefaultLabels({
  app: 'eltrello-analytics',
});

collectDefaultMetrics({register});

register.registerMetric(httpRequestTimer);
register.registerMetric(totalHttpRequestDuration);
register.registerMetric(totalHttpRequestCount);

app.use(cors());
app.use(addTraceId);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

mongoose.set("toJSON", {
  virtuals: true,
  transform: (_, converted) => {
    delete converted._id;
  },
});

app.get("/", (req, res) => {
  res.send("API is UP");
});

app.get('/metrics', async (req, res) => {
  res.setHeader('Content-Type', register.contentType);
  res.send(await register.metrics());
});

logger.warn({ message: `Hello World`, labels: { 'app': 'loki', 'origin': 'analytics' } })

app.post("/api/analytics", boardsController.createBoard);
app.get("/api/analytics", boardsController.getAnalytics);
app.get('/health', (req, res) => {
  return res.status(200).send({ message: "Health is good" });
});

mongoose.connect("mongodb://root:example@mongo:27017/eltrello-analytics?authSource=admin").then(() => {
  console.log("connected to mongodb");
  httpServer.listen(4002, () => {
    console.log(`API is listening on port 4002`);
  });
});
