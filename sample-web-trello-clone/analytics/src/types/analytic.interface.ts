import { Schema, Document } from "mongoose";

export interface Analytics {
  datapoint: string;
  value: string;
  createdAt: Date;
  updatedAt: Date;
  userId: Schema.Types.ObjectId;
}

export interface AnalyticsDocument extends Document, Analytics {}
