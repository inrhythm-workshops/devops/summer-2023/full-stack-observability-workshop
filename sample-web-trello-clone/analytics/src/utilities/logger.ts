import { createLogger, transports, format } from 'winston';
import LokiTransport from 'winston-loki';

export const logger = createLogger({
    transports: [
        new LokiTransport({
            host: "http://loki:3100",
            labels: { app: 'eltrello'},
            json: true,
            format: format.json(),
            replaceTimestamp: true,
            onConnectionError: (err) => {
                console.log("erroring1")
                console.error(err)
            }
        }),
        new transports.Console({
            format: format.combine(format.simple(), format.colorize())
        })
    ]
});