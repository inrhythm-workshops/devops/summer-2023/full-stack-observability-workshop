import { Schema, model } from "mongoose";
import { AnalyticsDocument } from "../types/analytic.interface";

const analyticSchema = new Schema<AnalyticsDocument>({
  datapoint: {
    type: String,
    required: true,
  },
  value: {
    type: String,
  },
  userId: {
    type: Schema.Types.ObjectId,
    required: true,
  },
});

export default model<AnalyticsDocument>("Analytic", analyticSchema);
