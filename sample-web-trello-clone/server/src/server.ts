import express, { Router } from "express";
import { createServer } from "http";
import { Server } from "socket.io";
import { Socket } from "./types/socket.interface";
import mongoose from "mongoose";
import jwt from "jsonwebtoken";
import * as usersController from "./controllers/users";
import * as boardsController from "./controllers/boards";
import * as columnsController from "./controllers/columns";
import * as tasksController from "./controllers/tasks";
import bodyParser from "body-parser";
import authMiddleware from "./middlewares/auth";
import cors from "cors";
import { SocketEventsEnum } from "./types/socketEvents.enum";
import { secret } from "./config";
import User from "./models/user";
import { collectDefaultMetrics, Registry, Histogram } from 'prom-client';
import { logger } from './utilities/logger';
import opentelemetry from '@opentelemetry/api';
import { tracer, addTraceId } from './utilities/tracing-tempo';
import './utilities/instrumentation';
// import { context, setSpan, getSpan } from '@opentelemetry/api';
import { httpRequestTimer, totalHttpRequestDuration, totalHttpRequestCount } from "./utilities/prometheus";

const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer, {
  cors: {
    origin: "*",
  },
});

// Create a Registry to register the metrics
const register = new Registry();
register.setDefaultLabels({
  app: 'eltrello',
});

collectDefaultMetrics({register});

register.registerMetric(httpRequestTimer);
register.registerMetric(totalHttpRequestDuration);
register.registerMetric(totalHttpRequestCount);

app.use(cors());
app.use(addTraceId);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

mongoose.set("toJSON", {
  virtuals: true,
  transform: (_, converted) => {
    delete converted._id;
  },
});

app.get("/", (req, res) => {
  res.send("API is UP");
});

app.get('/metrics', async (req, res) => {
  res.setHeader('Content-Type', register.contentType);
  res.send(await register.metrics());
});


logger.warn({ message: `Hello World`, labels: { 'app': 'loki', 'origin': 'api' } })

app.post("/api/users", usersController.register);
app.post("/api/users/login", usersController.login);
app.get("/api/user", authMiddleware, usersController.currentUser);
app.get("/api/boards", authMiddleware, boardsController.getBoards);
app.get("/api/boards/:boardId", authMiddleware, boardsController.getBoard);
app.get(
  "/api/boards/:boardId/columns",
  authMiddleware,
  columnsController.getColumns
);
app.get("/api/boards/:boardId/tasks", authMiddleware, tasksController.getTasks);
app.post("/api/boards", authMiddleware, boardsController.createBoard);
app.get('/health', (req, res) => {
  const parentSpan = opentelemetry.trace.getSpan(opentelemetry.context.active());
  doSomeWorkInNewSpan(parentSpan);

  return res.status(200).send({ message: "Health is good" });
});

const doSomeWorkInNewSpan = (parentSpan: any) => {

  //const ctx = setSpan(context.active(), parentSpan);
  //const childSpan = tracer.startSpan('doWork', undefined, ctx);
  const childSpan = tracer.startSpan('doSomeWorkInNewSpan', {
      attributes: { 'code.function' : 'doSomeWorkInNewSpan' }
  }, opentelemetry.context.active());

  childSpan.setAttribute('code.filepath', "test");
  doSomeWorkInNewNestedSpan(childSpan);
  childSpan.end();
}

const doSomeWorkInNewNestedSpan = (parentSpan: any) => {

  const ctx = opentelemetry.trace.setSpan(opentelemetry.context.active(), parentSpan);   
  const childSpan = tracer.startSpan('doSomeWorkInNewNestedSpan', {
      attributes: { 'code.function' : 'doSomeWorkInNewNestedSpan' }
  }, ctx);

  childSpan.setAttribute('code.filepath', "test2");
  opentelemetry.context.with(opentelemetry.trace.setSpan(opentelemetry.context.active(), childSpan), doSomeWorkInNewNested2Span);
  childSpan.end();
}

const doSomeWorkInNewNested2Span = () => {
  const childSpan = tracer.startSpan('doSomeWorkInNewNested2Span');

  logger.info({ message: `doSomeWorkInNewNested2Span`, labels: { 'app': 'loki', 'origin': 'api', 'traceID': childSpan.spanContext().traceId, 'spanID': childSpan.spanContext().spanId } })

  Promise.all([asyncWorkOne(childSpan), asyncWorkTwo(childSpan)])
          .then(results => logger.log(results as any))
          .catch(err => {
              logger.error(err);
          }).finally(() => {
              childSpan.end();
          });    
}

function asyncWorkOne(parentSpan: any) {    
  let childSpan: any;
  let promise = new Promise((resolve, reject) => {
      try {
          
          const ctx = opentelemetry.trace.setSpan(opentelemetry.context.active(), parentSpan);   
          childSpan = tracer.startSpan('asyncWorkOne', {
              attributes: { 'code.function' : 'asyncWorkOne' }
          }, ctx);
          resolve("promise 1 done!")
      } catch (e) {
          reject(e);
      } finally {
          childSpan.end();
      }
  });
  return promise;
}

function asyncWorkTwo(parentSpan: any) {

  let promise = new Promise((resolve, reject) => {
    let childSpan: any;
      try {
          const ctx = opentelemetry.trace.setSpan(opentelemetry.context.active(), parentSpan);   
          childSpan = tracer.startSpan('asyncWorkTwo', {
              attributes: { 'code.function' : 'asyncWorkTwo' }
          }, opentelemetry.context.active());
          resolve("promise 2 done!");
      } catch (e) {
          reject(e);
          
      } finally {
          childSpan.end();
      }
  });
  return promise;
}

io.use(async (socket: Socket, next) => {
  try {
    const token = (socket.handshake.auth.token as string) ?? "";
    const data = jwt.verify(token.split(" ")[1], secret) as {
      id: string;
      email: string;
    };
    const user = await User.findById(data.id);

    if (!user) {
      return next(new Error("Authentication error"));
    }
    socket.user = user;
    next();
  } catch (err) {
    next(new Error("Authentication error"));
  }
}).on("connection", (socket) => {
  socket.on(SocketEventsEnum.boardsJoin, (data) => {
    boardsController.joinBoard(io, socket, data);
  });
  socket.on(SocketEventsEnum.boardsLeave, (data) => {
    boardsController.leaveBoard(io, socket, data);
  });
  socket.on(SocketEventsEnum.columnsCreate, (data) => {
    columnsController.createColumn(io, socket, data);
  });
  socket.on(SocketEventsEnum.tasksCreate, (data) => {
    tasksController.createTask(io, socket, data);
  });
  socket.on(SocketEventsEnum.boardsUpdate, (data) => {
    boardsController.updateBoard(io, socket, data);
  });
  socket.on(SocketEventsEnum.boardsDelete, (data) => {
    boardsController.deleteBoard(io, socket, data);
  });
  socket.on(SocketEventsEnum.columnsDelete, (data) => {
    columnsController.deleteColumn(io, socket, data);
  });
  socket.on(SocketEventsEnum.columnsUpdate, (data) => {
    columnsController.updateColumn(io, socket, data);
  });
  socket.on(SocketEventsEnum.tasksUpdate, (data) => {
    tasksController.updateTask(io, socket, data);
  });
  socket.on(SocketEventsEnum.tasksDelete, (data) => {
    tasksController.deleteTask(io, socket, data);
  });
});

mongoose.connect("mongodb://root:example@mongo:27017/eltrello?authSource=admin").then(() => {
  console.log("connected to mongodb");
  httpServer.listen(4001, () => {
    console.log(`API is listening on port 4001`);
  });
});
