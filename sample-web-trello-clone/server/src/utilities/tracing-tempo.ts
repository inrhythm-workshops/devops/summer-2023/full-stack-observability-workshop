const opentelemetry = require("@opentelemetry/api");
import {NodeTracerProvider} from '@opentelemetry/sdk-trace-node'
import {registerInstrumentations} from '@opentelemetry/instrumentation'
import {JaegerExporter} from '@opentelemetry/exporter-jaeger'
import { BatchSpanProcessor } from '@opentelemetry/sdk-trace-base';
import { Resource } from '@opentelemetry/resources';
import { SemanticResourceAttributes } from '@opentelemetry/semantic-conventions';
import { ExpressInstrumentation } from '@aspecto/opentelemetry-instrumentation-express'
import { HttpInstrumentation } from '@opentelemetry/instrumentation-http'
import { logger } from './logger';

logger.level = "debug";

// Enable OpenTelemetry exporters to export traces to Grafan Tempo.

const tracerProvider = new NodeTracerProvider({
    resource: new Resource({
        [SemanticResourceAttributes.SERVICE_NAME]: 'eltrello',
    }),
});

registerInstrumentations({
    tracerProvider: tracerProvider,
    instrumentations: [
        new HttpInstrumentation()
    ]
});

// Initialize the exporter. 
const options = {
    serviceName: process.env.OTEL_SERVICE_NAME || "eltrello",
    tags: [], // optional
    // You can use the default UDPSender
    //host: 'localhost', // optional
    //port: 6832, // optional
    // OR you can use the HTTPSender as follows
    //14250 : model.proto not working 
    endpoint: process.env.OTEL_EXPORTER_JAEGER_ENDPOINT,
    maxPacketSize: 65000 // optional
}

/**
 * 
 * Configure the span processor to send spans to the exporter
 * The SimpleSpanProcessor does no batching and exports spans
 * immediately when they end. For most production use cases,
 * OpenTelemetry recommends use of the BatchSpanProcessor.
 */
tracerProvider.addSpanProcessor(new BatchSpanProcessor(new JaegerExporter(options) as any));
//tracerProvider.addSpanProcessor(new SimpleSpanProcessor(new ConsoleSpanExporter()));

/**
 * Registering the provider with the API allows it to be discovered
 * and used by instrumentation libraries. The OpenTelemetry API provides
 * methods to set global SDK implementations, but the default SDK provides
 * a convenience method named `register` which registers same defaults
 * for you.
 *
 * By default the NodeTracerProvider uses Trace Context for propagation
 * and AsyncHooksScopeManager for context management. To learn about
 * customizing this behavior, see API Registration Options below.
 */
// Initialize the OpenTelemetry APIs to use the NodeTracerProvider bindings
tracerProvider.register();

export const tracer = opentelemetry.trace.getTracer(process.env.OTEL_SERVICE_NAME as string);
console.log('-----------------------------------', tracer)
export const addTraceId = (req: any, res: any, next: any) => {
    const spanContext = opentelemetry.trace.getSpanContext(opentelemetry.context.active());
    req.traceID = spanContext && spanContext.traceId;    
    console.log("//////////////////////////////////////", spanContext)
    logger.info({ message: `Added Trace Id`, labels: { 'app': 'loki', 'origin': 'api', 'traceID': req.traceID } })
    next();
};

logger.debug({ 
    message: `tracing initialized for ${options.serviceName} sending span to ${options.endpoint}`, 
    labels: { 'app': 'loki', 'origin': 'api' }
});
