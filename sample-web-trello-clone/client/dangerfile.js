import { message, warn, fail, danger } from 'danger';
import { exec } from 'child_process';
import * as path from 'path';

// TSLint
const runTSLint = () => {
  const modifiedFiles = danger.git.modified_files
    .filter((file) => /\.ts[x]?$/.test(file))
    .join(' ');
  const tsLintExec = path.join(__dirname, 'node_modules', '.bin', 'tslint');
  const tslintCommand = `${tsLintExec} ${modifiedFiles} -t prose --project tsconfig.json`;
  const tslintOut = exec(tslintCommand, (error, stdout, stderr) => {
    if (error) {
      warn(stdout || stderr);
    }
  });
};
runTSLint();
