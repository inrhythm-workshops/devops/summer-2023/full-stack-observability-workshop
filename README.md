## InRhythm Summer Summit 2023 Workshop: Fullstack Observability

### Prerequisites
* docker
* docker-compose

### Getting Started
1. Run `docker-compose up` to start services inside docker-compose i.e. Prometheus and Grafarana